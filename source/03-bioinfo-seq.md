- Aim: obtaining contigs (group of sequence, i.e. forward + reverse)
- Satu reaksi Sanger adalah satu aktivitas (forward/reverse saja)
- Saat melakukan Sanger sequencing, output file dalam format `ab1`
- Prinsip Sanger
  - Fluoroscence dye, tiap basa akan memiliki warna yang berbeda
  - Chromatogram akan menunjukkan perbedaan dari fluoroscence dye
- Sanger optimal untuk membaca hingga 700-800 bp, idealnya 500 bp saja
- Sanger is single read, while NGS is multi-read
- Selalu sediakan "ruang" esktra di depan dan akhir untuk trimming $\to$ Dengan
  metode Sanger, bagian awal dan akhir memiliki kualitas yang rendah
- Reverse complement dilakukan setelah trimming
