- Agar DNA telihat jelas saat elektroforesis, maka membutuhkan konsentrasi 500
  nanogram
- Penyebab elektroforesis gagal visulisasi:
  - DNA sudah terdegradasi
  - Konsentrasi DNA terlalu rendah
- Secara kuantitatif $\to$ Dapat dilihat absorbansi $\to$ Meninjau tingkat
  kekeruhan
  - Asam nukleat dapat terlihat pada panjang gelombang 260
  - Protein dapat terlihat pada panjang gelombang 280
  - Panjang gelombang 260 **tidak** hanya DNA, bisa juga RNA dan lainnya
  - Perlu dibandingkan rasio deteksi panjang gelombang 260 dan 280
    - Rasio yang baik adl 2 $\to$ Jumlah asam nukleat 2x jumlah protein
    - Rasio yang terlalu besar $\to$ Ada kontaminan organik
    - Rasio yang terlalu kecil $\to$ Jumlah protein terlalu banyak, mungkin
      saja ada enzim nuklease
- Optimasi primer bergantung dari produsen
  - Annealing temperature $\to$ Recall that sequencing requires denaturation,
    annealing, and extension/elongation
  - Do gradient PCR $\to$ Can evaluate multiple annealing temperatures
  - Untuk CYP2C19\*2/\*3/\*7 $\to$ Optimal annealing temperature pada 59 ^o^C
