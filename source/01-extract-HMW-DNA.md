- Yang dimaksud sebagai high-molecular weight (HMW) adalah rantai base pair
  yang panjang pada DNA
- Treatment terhadap DNA manusia agak berbeda dengan virus atau bakteri $\to$
  Virus dan bakteri termasuk low-molecular weight (LMW)
- Secara prinsip DNA dapat diambil dari mana pun
  - Blood
  - Semen
  - Saliva
  - Urine
  - Hair
  - Teeth
  - Bone
  - Tissue
- Essential components of DNA extraction:
  - Maximize DNA discovery
    - Diploid cell: 6 pg (picogram) DNA
    - Sperm: 3 pg DNA
  - Remove inhibitors
  - Remove/inhibit nucleases $\to$ An enzyme breaking down the DNA
  - Maximize the quantity of DNA
- RFLP procedure requires at least 50 ng DNA (2 uL)
- PCR requires on average 1 ng DNA
- Common DNA extraction methods:
  - Organic $\to$ Use phenol and/or chloroform to purify the DNA
    - Yields relatively pure HMW DNA
    - Cell lysis buffer $\to$ Acquire pellet
    - Resuspend nuclei
    - Remove proteinaceous material using phenol-chloroform $\to$ DNA
      extraction
    - Reagents: Phenol, chloroform, hydoxquinoline
    - Concentrating DNA with alcohol precipitation $\to$ Ethanol, isopropanol
  - Anorganic
    - Salting out with high concentration of LiCl
- Setiap kali thawing, terjadi degradasi
- DNA dapat disimpan dalam bentuk kering $\to$ Seperti bubuk putih, dapat
  disimpan lebih lama
- Electrophoresis with agarose gel $\to$ Menunjukkan fragmentasi dari DNA
- Buffy coat mengumpulkan sel berinti pada darah $\to$ Higher DNA discovery
