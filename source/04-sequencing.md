- Deoxy: Start coding $\to$ dATP, dTTP, dGTP, dCTP
- Di-deoxy: Stop coding $\to$ ddATP, ddTTP, ddGTP, ddCTP
- ONT: Merubah current menjadi sequence berdasarkan perubahan current akibat
  perbedaan jenis sequence genetik
