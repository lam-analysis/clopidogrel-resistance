#!/bin/bash


## ENV

# Set directory variables
ID=221207-ONT
BASE=data/raw/$ID/fastq_pass
REF=data/ref/NG_008384.fasta
SAMDIR=data/processed/$ID/SAM


## FUNC

# Concatenate all FASTA within a barcode folder
function catfasta () {
    BARFILE=$BASE/$BARID.fastq.gz
    cat $BASE/$BARID/*.fastq.gz > $BARFILE
    echo $BARFILE
}

# Map genomic alignment with minimap2
function mapfasta () {
    SAMFILE=$SAMDIR/$BARID.sam
    minimap2 --MD -a $REF $BARFILE > $SAMFILE
}


## MAP

# Create SAM directory if not available
if [[ ! -d $SAMDIR ]]; then
    echo "Creating $SAMDIR"
    mkdir -p $SAMDIR
else
    echo "$SAMDIR is available"
fi

# Looping over directories to map sequence of barcodes
for BARID in $(ls $BASE); do
    echo "Concatenate $BASE/$BARID"
    BARFILE=$(catfasta)
    echo "Mapping $BARID sequence to $REF"
    mapfasta
    rm $BARFILE
    echo "Result: $BARFILE"
done


## REPORT

echo "The total mapped SAM is $(du --summarize -h $SAMDIR)"
