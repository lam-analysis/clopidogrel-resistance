#!/bin/bash


## ENV

# Set directory variables
ID=221207-ONT
BASE=data/raw/$ID/fastq_pass
VCFDIR=data/processed/$ID/VCF
BAMSORTDIR=data/processed/$ID/BAM-sort


## VARCALL

# Create VCF directory if not available
if [[ ! -d $VCFDIR ]]; then
    echo "Creating $VCFDIR"
    mkdir -p $VCFDIR
else
    echo "$VCFDIR is available"
fi

# Looping after sorted BAM to create VCF
for BARID in $(ls $BASE); do
    BAMFILE=$BAMSORTDIR/$BARID-sort.bam
    VCFFILE=$VCFDIR/$BARID.vcf
    echo "Variant calling from the sorted BAM files"
    sniffles -m $BAMFILE -v $VCFFILE
done
