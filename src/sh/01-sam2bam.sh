#!/bin/bash


## ENV

# Set directory variables
ID=221207-ONT
SAMDIR=data/processed/$ID/SAM
BAMDIR=data/processed/$ID/BAM
BAMSORTDIR=data/processed/$ID/BAM-sort


## CONVERT

# Create BAM directory if not available
if [[ ! -d $BAMDIR ]]; then
    echo "Creating $BAMDIR and $BAMSORTDIR"
    mkdir -p $BAMDIR
    mkdir -p $BAMSORTDIR
else
    echo "$BAMDIR and $BAMSORTDIR are available"
fi

# Iterate through SAM files to generate BAM
for SAMFILE in $(ls $SAMDIR); do
    BAMFILE=$BAMDIR/$(sed 's/sam/bam/' <(echo $SAMFILE))
    BAMSORTFILE=$BAMSORTDIR/$(sed 's/.sam/-sort.bam/' <(echo $SAMFILE))
    echo "Converting SAM to BAM"
    samtools view -bS $SAMDIR/$SAMFILE > $BAMFILE
    echo -e "Produced $BAMFILE\nSorting the BAM file"
    samtools sort -o $BAMSORTFILE $BAMFILE
    echo "Creating an index file"
    samtools index $BAMSORTFILE
    echo "Produced $BAMSORTFILE"
done
