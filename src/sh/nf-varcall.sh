#!/bin/bash

## ENV

DIR="data"
ID=$(ls $DIR/raw | fzf --prompt="Type in to filter: ")
RAW="$DIR/raw/$ID"
RES="$DIR/processed/$ID"
REF="$DIR/ref/hg38.fa"
TGT="$DIR/ref/target.bed"
CORE=8

# Create target/bed file
 #Information for target/bed file is acquired from the following URL:
 #https://www.ncbi.nlm.nih.gov/nuccore/NC_000010.11?report=fasta&from=94762681&to=94855547
echo -e "chr10\t94762681\t94855547" > $TGT

# Create the result path if not available
clear
echo "Preparing the $RES directory"

if [ -d $RES ]; then
    echo "$RES exists, deleting"
    sudo rm -rI $RES
else
    echo "Creating $RES"
fi

mkdir -p "$RES/SNP"


## ANALYZE

# Alignment
for FASTQ in $(ls -d $RAW/fastq_pass/*/); do
    clear
    echo "Analyzing $FASTQ"
    BARID="$(basename $FASTQ)"
    OUTDIR="$RES/BAM-sort/$BARID/"
    nextflow run epi2me-labs/wf-alignment \
        --fastq $FASTQ \
        --references $REF \
        --out_dir $OUTDIR
    find $OUTDIR ! -name "*.bam*" -delete
    mv $OUTDIR/* $RES/BAM-sort/
    [ -d $OUTDIR ] && rm -r $OUTDIR
    echo "Clean up Nextflow working directory"
    [ -d work ] && rm -r work
done

# Variant calling
for BAM in $(find $RES/BAM-sort -name "*.sorted.aligned.bam"); do
    clear
    echo "Processing $BAM"
    BAMID="$(basename $BAM)"
    BARID="${BAMID%%.*}"
    OUTDIR="$RES/VCF/"
    nextflow run epi2me-labs/wf-human-variation \
        --threads $CORE \
        --snp \
        --ref $REF \
        --bed $TGT \
        --out_dir $OUTDIR \
        --sample_name $BARID \
        --bam $BAM
    find $OUTDIR ! -name "*.vcf*" -delete
    echo "Clean up Nextflow working directory"
    [ -d work ] && rm -r work
done

# Variant annotation using PharmCAT
docker pull pgkb/pharmcat

for VCF in $(find $RES/VCF -regex ".*vcf\(.gz\)?" | xargs -n1 basename); do
    clear
    BASE="${VCF%%.*}"
    echo "Annotating $BASE"
    docker run --rm -v \
        $(readlink -f $RES/VCF):/pharmcat/data pgkb/pharmcat \
        ./pharmcat_pipeline -o data/CAT/$BASE data/$VCF 
done

# Extract and concatenate SNP in all annotated barcodes
clear
for BAR in $(find $RES/VCF/CAT -type d -iname "*barcode*"); do
    echo "Extracting SNP from $BAR"
    BARID=$(basename $BAR)
    OUTFILE="$RES/SNP/$BARID.jsonl"
    jq -c \
        '.geneReports[].CYP2C19.recommendationDiplotypes[] |
        {snp: .label, label: .phenotypes[]}' \
            $BAR/*pheno*json > $OUTFILE
    echo "Concatenated as $OUTFILE"
done

# Call the R Script to process extracted SNP
Rscript src/R/02-vcf-annotation.R "$ID"
