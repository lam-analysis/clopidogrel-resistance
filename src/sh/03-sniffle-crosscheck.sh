#!/bin/bash

## ENV

# Prepare environment variables
DIR="data"
ID=$(ls $DIR/raw | fzf --prompt="Type in to filter: ")
RAW="$DIR/raw/$ID"
RES="$DIR/processed/$ID"
REF="$DIR/ref/hg38.fa"
BARID=$(ls $RAW/fastq_pass | fzf --prompt="Select the barcode ID to cross-check: ")
SNFPATH=$RES/sniffles-xcheck

# Create directories if empty, else remove the previous path
if [ -d $SNFPATH ]; then
    echo "Deleting old directory"
    rm -r $SNFPATH
fi

echo "Creating directory"
mkdir -p $SNFPATH/{fastq,sam,bam,vcf}


## ANALYZE

# Concatenate all FASTQ files
echo "Concatenate all FASTQ files in $BARID"
zcat $RAW/fastq_pass/$BARID/*fastq.gz > $SNFPATH/fastq/$BARID.fastq

# Map FASTQ as SAM file
echo "Map $BARID FASTQ as SAM"
minimap2 --MD -a $REF $SNFPATH/fastq/$BARID.fastq > $SNFPATH/sam/$BARID.sam

# Create, sort, and index the BAM file
echo "Create BAM from SAM for $BARID"
samtools view -bS $SNFPATH/sam/$BARID.sam > $SNFPATH/bam/$BARID.bam
echo "Sort and index $BARID BAM"
samtools sort -o $SNFPATH/bam/$BARID-sorted.bam $SNFPATH/bam/$BARID.bam
samtools index $SNFPATH/bam/$BARID-sorted.bam

# Cross-check using sniffles
echo "Variant calling using sniffles"
SORTED_BAM="$SNFPATH/bam/$BARID-sorted.bam"
sniffles -m $SORTED_BAM -v $SNFPATH/vcf/$BARID.vcf
