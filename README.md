# Background

In this repository, I stashed required scripts to perform bioinformatics analysis for clopidogrel resistance genomic investigation in the National Brain Centre Hospital (*RS Pusat Otak Nasional*), Jakarta. To use the script, you will need to provide the ONT sequencing results into the respective raw data directory, e.g. `data/raw/230119-ONT` for ONT results obtained on 19 Jan 2023. Afterwards, you just have to run the bash script as provided in `src/sh/` directory.

# Getting started

## First time using this repository

1. Setup `conda` environment  
   ```bash
   conda create --name bioinfo
   conda activate bioinfo
   ```
2. Install bioinformatics tools from Bioconda channel  
   ```bash
   conda install -c bioconda minimap2 samtools sniffles
   ```
3. Clone the repository and change the working directory  
   ```bash
   git clone https://gitlab.com/lam-analysis/clopidogrel-resistance/
   cd clopidogrel-resistance
   ```
4. Move ONT sequencing results into the raw data directory
   ```bash
   mv /path/to/old/dir data/raw/newdirname
   ```
5. Run the bash script (e.g. to perform sequence alignment)
   ```bash
   bash src/sh/00-seq-alignment.sh
   bash src/sh/01-sam2bam.sh
   bash src/sh/02-varcall.sh
   ```

## The next time using this repository

1. Pull changes from remote repository to update your local scripts
   ```bash
   git pull --rebase --set-upstream origin master
   ```
2. Run the bash script
   ```bash
   bash src/sh/00-seq-alignment.sh
   bash src/sh/01-sam2bam.sh
   bash src/sh/02-varcall.sh
   ```

## Analyzing data from different folder

By editing the provided bash scripts, you can always opt to analyze raw data from *any* folder. However, to keep your working directory clean, I'd suggest using the `data/raw` directory to contain subfolders for analysis. To our convenient, we shall use `YYMMDD-ONT` naming convention for the subfolders, e.g. `230119-ONT`, where `YY` is two last digits of the current year of sequencing, `MM` is the months of sequencing, and `DD` is the date of sequencing. The suffix `ONT` implies that we are using genetic sequences obtained from ONT devices. This will leave us a flexibility to reorganize the subfolder into different date of sequencing and different machines.

To analyze a different subfolder other than `230119-ONT`, first you will need to get all the raw sequencing data into `data/raw/your-subfolder-name`. Ideally, `your-subfolder-name` should contain the following structure:

```
your-subfolder-name
└── fastq_pass
    ├── barcode01
    │   ├── FAU09232_pass_barcode01_9fcec965_71627896_0.fastq.gz
    │   ├── FAU09232_pass_barcode01_9fcec965_71627896_1.fastq.gz
    │   ├── . . .
    │   └── FAU09232_pass_barcode01_9fcec965_71627896_n.fastq.gz
    ├── barcode02
    │   ├── FAU09232_pass_barcode02_9fcec965_71627896_0.fastq.gz
    │   ├── . . .
    │   └── FAU09232_pass_barcode02_9fcec965_71627896_n.fastq.gz
    │
    ├── . . .
    │
    ├── barcode86
    │   ├── FAU09232_pass_barcode86_9fcec965_71627896_0.fastq.gz
    │   ├── . . .
    │   └── FAU09232_pass_barcode86_9fcec965_71627896_n.fastq.gz
    └── unclassified
        └── FAU09232_pass_unclassified_9fcec965_71627896_9.fastq.gz
```

By default, bash scripts in `src/sh` will iterate the `fastq_pass` directory, so you need to make sure that you have the directory included in `your-subfolder-name`. After structuring `your-subfolder-name`, you will need to *edit* the bash script. Look for the part under `## ENV`, then edit `ID=230119-ONT` into `ID=your-subfolder-name`. Finally, you can re-run the analysis by running:


```bash
bash src/sh/00-seq-alignment.sh
bash src/sh/01-sam2bam.sh
bash src/sh/02-varcall.sh
```
